package logger

import (
	"encoding/json"
	"fmt"
	"log"
	"time"

	"gitlab.com/pixie-sdk/go/microservice/base"
)

type Logger struct {
	source string
}

func NewLogger(source string) *Logger {
	return &Logger{
		source: source,
	}
}

type LoggerWithTrace struct {
	Logger

	traceID string
}

const (
	LevelDebug = 1
	LevelWarn  = 2
	LevelError = 3
	LevelInfo  = 4
	LevelFatal = 5
)

type Search struct {
	// request
	Request struct {
		// required
		Page     int `json:"page"`
		PageSize int `json:"pageSize"`
		// optional
		Search struct {
			Source         string `json:"source"`
			Level          int    `json:"level"`
			LevelType      string `json:"levelType"`
			Path           string `json:"path"`
			TraceID        string `json:"traceId"`
			Message        string `json:"message"`
			TimestampStart int64  `json:"timestampStart"`
			TimestampEnd   int64  `json:"timestampEnd"`
			ValuesKey      string `json:"valuesKey"`
			ValuesValue    string `json:"valuesValue"`
		} `json:"search"`
	}

	// response
	Response struct {
		Code    int    `json:"code"`
		Message string `json:"message"`
		PageMax int64  `json:"pageMax"`
		Logs    []struct {
			Timestamp int64             `json:"timestamp"`
			Source    string            `json:"source"`
			Level     int               `json:"level"`
			Path      string            `json:"path"`
			TraceID   string            `json:"traceId"`
			Message   string            `json:"message"`
			Values    map[string]string `json:"values"`
		}
	}
}

func (l Logger) Search() *Search {
	return &Search{}
}

func (s *Search) UnmarshalRequest(payload []byte) *Search {
	json.Unmarshal(payload, &s.Request)
	return s
}

func (s *Search) Do(traceID string) error {
	if s.Request.Page <= 0 || s.Request.PageSize <= 0 {
		return fmt.Errorf("page and pageSize are required")
	}

	data, err := base.CallService("logger", "/log/search", s.Request, traceID)
	if err != nil {
		return fmt.Errorf("base.CallService, %v", err)
	}

	if err := json.Unmarshal(data, &s.Response); err != nil {
		return fmt.Errorf("failed to unmarshal response, %v, data: %s", err, string(data))
	}

	if s.Response.Code != 0 {
		return fmt.Errorf("failed to search log, code: %d, message: %s", s.Response.Code, s.Response.Message)
	}

	return nil
}

func (l Logger) push(level int, path string, traceID string, message string, values map[string]string) error {
	log := struct {
		Timestamp int64             `json:"timestamp"`
		Source    string            `json:"source"`
		Level     int               `json:"level"`
		Path      string            `json:"path"`
		TraceID   string            `json:"traceId"`
		Message   string            `json:"message"`
		Values    map[string]string `json:"values"`
	}{
		Timestamp: time.Now().UnixMilli(),
		Source:    l.source,
		Level:     level,
		Path:      path,
		TraceID:   traceID,
		Message:   message,
		Values:    values,
	}

	data, err := base.CallService("logger", "/log/push", log, traceID)
	if err != nil {
		return fmt.Errorf("failed to send request, %v", err)
	}

	var response struct {
		Code    int    `json:"code"`
		Message string `json:"message"`
	}

	if err := json.Unmarshal(data, &response); err != nil {
		return fmt.Errorf("failed to unmarshal response, %v, data: %s", err, string(data))
	}

	if response.Code != 0 {
		return fmt.Errorf("failed to push log, code: %d, message: %s", response.Code, response.Message)
	}

	return nil
}

func (l Logger) Debug(path string, message string) {
	log.Printf("[DEBUG] %s | %s", path, message)
	go func() {
		if err := l.push(LevelDebug, path, "", message, nil); err != nil {
			log.Printf("[DEBUG] %s | %s, %v", path, message, err)
		}
	}()
}

func (l Logger) Warn(path string, message string) {
	log.Printf("[WARN] %s | %s", path, message)
	go func() {
		if err := l.push(LevelWarn, path, "", message, nil); err != nil {
			log.Printf("[WARN] %s | %s, %v", path, message, err)
		}
	}()
}

func (l Logger) Error(path string, message string) {
	log.Printf("[ERROR] %s | %s", path, message)
	go func() {
		if err := l.push(LevelError, path, "", message, nil); err != nil {
			log.Printf("[ERROR] %s | %s, %v", path, message, err)
		}
	}()
}

func (l Logger) Info(path string, message string) {
	log.Printf("[INFO] %s | %s", path, message)
	go func() {
		if err := l.push(LevelInfo, path, "", message, nil); err != nil {
			log.Printf("[INFO] %s | %s, %v", path, message, err)
		}
	}()
}

func (l Logger) Fatal(path string, message string) {
	log.Printf("[FATAL] %s | %s", path, message)
	go func() {
		if err := l.push(LevelFatal, path, "", message, nil); err != nil {
			log.Printf("[FATAL] %s | %s, %v", path, message, err)
		}
	}()
}

func (l Logger) WithTrace(traceID string) LoggerWithTrace {
	return LoggerWithTrace{
		Logger: l,

		traceID: traceID,
	}
}

func (l LoggerWithTrace) Debug(path string, message string, values map[string]string) {
	log.Printf("[DEBUG] %s | %s | %s | %s", path, message, l.traceID, values)
	go func() {
		if err := l.push(LevelDebug, path, l.traceID, message, values); err != nil {
			log.Printf("[DEBUG] %s | %s | %s | %s, %v", path, message, l.traceID, values, err)
		}
	}()
}

func (l LoggerWithTrace) Warn(path string, message string, values map[string]string) {
	log.Printf("[WARN] %s | %s | %s | %s", path, message, l.traceID, values)
	go func() {
		if err := l.push(LevelWarn, path, l.traceID, message, values); err != nil {
			log.Printf("[WARN] %s | %s | %s | %s, %v", path, message, l.traceID, values, err)
		}
	}()
}

func (l LoggerWithTrace) Error(path string, message string, values map[string]string) {
	log.Printf("[ERROR] %s | %s | %s | %s", path, message, l.traceID, values)
	go func() {
		if err := l.push(LevelError, path, l.traceID, message, values); err != nil {
			log.Printf("[ERROR] %s | %s | %s | %s, %v", path, message, l.traceID, values, err)
		}
	}()
}

func (l LoggerWithTrace) Info(path string, message string, values map[string]string) {
	log.Printf("[INFO] %s | %s | %s | %s", path, message, l.traceID, values)
	go func() {
		if err := l.push(LevelInfo, path, l.traceID, message, values); err != nil {
			log.Printf("[INFO] %s | %s | %s | %s, %v", path, message, l.traceID, values, err)
		}
	}()
}

func (l LoggerWithTrace) Fatal(path string, message string, values map[string]string) {
	log.Printf("[FATAL] %s | %s | %s | %s", path, message, l.traceID, values)
	go func() {
		if err := l.push(LevelFatal, path, l.traceID, message, values); err != nil {
			log.Printf("[FATAL] %s | %s | %s | %s, %v", path, message, l.traceID, values, err)
		}
	}()
}
