package misc

import "github.com/Lee-Chi/go-sdk/http"

func PostJson(url string, header map[string]string, data interface{}) ([]byte, error) {
	request := http.NewRequest()
	for key, value := range header {
		request.SetHeader(key, value)
	}

	return request.PostJson(url, data)
}

func Get(traceID string, url string) ([]byte, error) {
	return http.NewRequest().SetHeader("traceId", traceID).Get(url)
}
