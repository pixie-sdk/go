package model

const CAuth string = "auth"

type Auth struct {
	Name string `bson:"name"`
	Url  string `bson:"url"`
}
