package auth

import (
	"context"
	"fmt"

	"github.com/Lee-Chi/go-sdk/password"
	"gitlab.com/pixie-sdk/go/microservice/auth/db"
	"gitlab.com/pixie-sdk/go/microservice/auth/db/model"
	"go.mongodb.org/mongo-driver/bson/primitive"
)

type Auth struct {
	ID         primitive.ObjectID `bson:"_id"`
	model.Auth `bson:"-,inline"`
}

var serviceID primitive.ObjectID
var auths map[string]Auth

func Init(ctx context.Context, dbUri string, serviceName string, serviceUrl string) error {
	if err := db.Build(ctx, dbUri, "service"); err != nil {
		return err
	}

	var auth Auth
	if err := db.Collection(model.CAuth).Where(model.Key.Auth.Name.Eq(serviceName)).FindOneOrZero(ctx, &auth); err != nil {
		return err
	}

	if auth.ID.IsZero() {
		auth.Name = serviceName
		auth.Url = serviceUrl
		id, err := db.Collection(model.CAuth).InsertOne(
			ctx,
			auth.Auth,
		)
		if err != nil {
			return err
		}

		auth.ID = id
	}

	serviceID = auth.ID

	auths = make(map[string]Auth)

	return nil
}

type ServiceAuth struct {
	Name string
	Auth string
	Url  string
}

func Create(serviceName string, refresh bool) (*ServiceAuth, error) {
	if auths == nil {
		return nil, fmt.Errorf("auths is nil, must call Init first")
	}

	auth, ok := auths[serviceName]
	if refresh || !ok {
		if err := db.Collection(model.CAuth).Where(model.Key.Auth.Name.Eq(serviceName)).FindOne(context.Background(), &auth); err != nil {
			return nil, err
		}
	}

	hash, err := password.Encrypt(auth.ID.Hex())
	if err != nil {
		return nil, err
	}

	return &ServiceAuth{
		Name: auth.Name,
		Auth: hash,
		Url:  auth.Url,
	}, nil
}

func Verify(auth string) error {
	if err := password.Verify(serviceID.Hex(), auth); err != nil {
		return err
	}

	return nil
}

func Destroy() error {
	ctx := context.Background()

	// if !serviceID.IsZero() {
	// 	if err := db.Collection(model.CAuth).Where(model.Key.Auth.ID.Eq(serviceID)).DeleteMany(ctx); err != nil {
	// 		return err
	// 	}
	// }

	if err := db.Destroy(ctx); err != nil {
		return err
	}

	return nil
}
