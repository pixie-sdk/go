package task

import (
	"encoding/json"
	"fmt"

	"gitlab.com/pixie-sdk/go/microservice/base"
)

type Group struct {
}

func (g Group) DoOnce() *APIDoOnce {
	return &APIDoOnce{}
}

type APIDoOnce struct {
	// request
	Request struct {
		LineUserID    string `json:"lineUserId"`
		Type          string `json:"type"`
		Event         string `json:"event"`
		AfterDuration int    `json:"afterDuration"`
	}
	// response
	Response struct {
		Code    int    `json:"code"`
		Message string `json:"message"`
		ID      string `json:"id"`
	}
}

func (api APIDoOnce) RequestSample() string {
	api.Request.Type = "xxx"
	api.Request.Event = "yyy"
	api.Request.AfterDuration = 0
	sample, _ := json.Marshal(api.Request)
	return string(sample)
}

func (api *APIDoOnce) UnmarshalRequest(payload []byte) *APIDoOnce {
	json.Unmarshal(payload, &api.Request)
	return api
}

func (api *APIDoOnce) Do(traceID string) error {
	if api.Request.LineUserID == "" || api.Request.Type == "" || api.Request.Event == "" {
		return fmt.Errorf("lineUserId or type or event are required")
	}

	data, err := base.CallService("pixie", "/api/line/task/do-once", api.Request, traceID)
	if err != nil {
		return fmt.Errorf("base.CallService, %v", err)
	}

	if err := json.Unmarshal(data, &api.Response); err != nil {
		return fmt.Errorf("failed to unmarshal response, %v, data: %s", err, string(data))
	}

	if api.Response.Code != 0 {
		return fmt.Errorf("api failed, code: %d, message: %s", api.Response.Code, api.Response.Message)
	}

	return nil
}
