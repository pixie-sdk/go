package user

import (
	"encoding/json"
	"fmt"

	"gitlab.com/pixie-sdk/go/microservice/base"
)

type Group struct {
}

func (g Group) Register() *APIRegister {
	return &APIRegister{}
}

type APIRegister struct {
	// request
	Request struct {
		Account  string `json:"account"`
		Password string `json:"password"`
		Username string `json:"username"`
	}
	// response
	Response struct {
		Code    int    `json:"code"`
		Message string `json:"message"`
	}
}

func (api APIRegister) RequestSample() string {
	api.Request.Account = "xxx"
	api.Request.Password = "xxx"
	api.Request.Username = "xxx"
	sample, _ := json.Marshal(api.Request)
	return string(sample)
}

func (api *APIRegister) UnmarshalRequest(payload []byte) *APIRegister {
	json.Unmarshal(payload, &api.Request)
	return api
}

func (api *APIRegister) Do(traceID string) error {
	if api.Request.Account == "" || api.Request.Password == "" || api.Request.Username == "" {
		return fmt.Errorf("account, password, username are required")
	}

	data, err := base.CallService("pixie", "/api/line/user/register", api.Request, traceID)
	if err != nil {
		return fmt.Errorf("base.CallService, %v", err)
	}

	if err := json.Unmarshal(data, &api.Response); err != nil {
		return fmt.Errorf("failed to unmarshal response, %v, data: %s", err, string(data))
	}

	if api.Response.Code != 0 {
		return fmt.Errorf("api failed, code: %d, message: %s", api.Response.Code, api.Response.Message)
	}

	return nil
}

func (g Group) Super() *APISuper {
	return &APISuper{}
}

type APISuper struct {
	// request
	Request struct {
		Account string `json:"account"`
		Is      bool   `json:"is"`
	}
	// response
	Response struct {
		Code    int    `json:"code"`
		Message string `json:"message"`
	}
}

func (api APISuper) RequestSample() string {
	api.Request.Account = "xxx"
	api.Request.Is = true
	sample, _ := json.Marshal(api.Request)
	return string(sample)
}

func (api *APISuper) UnmarshalRequest(payload []byte) *APISuper {
	json.Unmarshal(payload, &api.Request)
	return api
}

func (api *APISuper) Do(traceID string) error {
	if api.Request.Account == "" {
		return fmt.Errorf("account is required")
	}

	data, err := base.CallService("pixie", "/api/line/user/super", api.Request, traceID)
	if err != nil {
		return fmt.Errorf("base.CallService, %v", err)
	}

	if err := json.Unmarshal(data, &api.Response); err != nil {
		return fmt.Errorf("failed to unmarshal response, %v, data: %s", err, string(data))
	}

	if api.Response.Code != 0 {
		return fmt.Errorf("api failed, code: %d, message: %s", api.Response.Code, api.Response.Message)
	}

	return nil
}

func (g Group) Bind() *APIBind {
	return &APIBind{}
}

type APIBind struct {
	// request
	Request struct {
		Account    string `json:"account"`
		LineUserID string `json:"lineUserId"`
	}
	// response
	Response struct {
		Code    int    `json:"code"`
		Message string `json:"message"`
	}
}

func (api APIBind) RequestSample() string {
	api.Request.Account = "xxx"
	api.Request.LineUserID = "yyy"
	sample, _ := json.Marshal(api.Request)
	return string(sample)
}

func (api *APIBind) UnmarshalRequest(payload []byte) *APIBind {
	json.Unmarshal(payload, &api.Request)
	return api
}

func (api *APIBind) Do(traceID string) error {
	if api.Request.Account == "" || api.Request.LineUserID == "" {
		return fmt.Errorf("account and lineUserId are required")
	}

	data, err := base.CallService("pixie", "/api/line/user/bind", api.Request, traceID)
	if err != nil {
		return fmt.Errorf("base.CallService, %v", err)
	}

	if err := json.Unmarshal(data, &api.Response); err != nil {
		return fmt.Errorf("failed to unmarshal response, %v, data: %s", err, string(data))
	}

	if api.Response.Code != 0 {
		return fmt.Errorf("api failed, code: %d, message: %s", api.Response.Code, api.Response.Message)
	}

	return nil
}
