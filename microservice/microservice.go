package microservice

import (
	"context"
	"fmt"
	"net/http"

	"gitlab.com/pixie-sdk/go/microservice/ai"
	"gitlab.com/pixie-sdk/go/microservice/asset"
	"gitlab.com/pixie-sdk/go/microservice/auth"
	"gitlab.com/pixie-sdk/go/microservice/base"
	"gitlab.com/pixie-sdk/go/microservice/line"
	"gitlab.com/pixie-sdk/go/microservice/logger"
	"gitlab.com/pixie-sdk/go/microservice/pixie"
)

var (
	Pixie  *pixie.Pixie
	Logger *logger.Logger
	Asset  *asset.Asset
	Line   *line.Line
	AI     *ai.AI
)

func Init(ctx context.Context, authDBUri string, serviceName string, serviceUrl string) error {
	if err := auth.Init(ctx, authDBUri, serviceName, serviceUrl); err != nil {
		return fmt.Errorf("failed to init auth, %v", err)
	}

	return nil
}

func Destroy() {
	auth.Destroy()
}

func CheckAuthorization(header http.Header) error {
	authorization := header.Get("Authorization")

	return base.CheckAuthorization(authorization)
}

func InitLogger(source string) {
	Logger = logger.NewLogger(source)
}

func InitAsset(url string) {
	Asset = asset.NewAsset(url)
}

func InitPixie() {
	Pixie = pixie.NewPixie()
}

func InitLine() {
	Line = line.NewLine()
}

func InitAI() {
	AI = ai.NewAI()
}
