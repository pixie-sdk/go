package line

import (
	"encoding/json"
	"fmt"
	"sync"

	"gitlab.com/pixie-sdk/go/microservice/base"
)

type Line struct {
}

var line *Line
var once sync.Once

func NewLine() *Line {
	once.Do(func() {
		line = &Line{}
	})

	return line
}

func (l Line) Send() *APISend {
	return &APISend{}
}

type APISend struct {
	// request
	Request struct {
		To      string `json:"to"`
		Message string `json:"message"`
	}
	// response
	Response struct {
		Code    int    `json:"code"`
		Message string `json:"message"`
	}
}

func (api APISend) RequestSample() string {
	api.Request.To = "xxx"
	api.Request.Message = "yyy"
	sample, _ := json.Marshal(api.Request)
	return string(sample)
}

func (api *APISend) UnmarshalRequest(payload []byte) *APISend {
	json.Unmarshal(payload, &api.Request)
	return api
}

func (api *APISend) Do(traceID string) error {
	if api.Request.To == "" || api.Request.Message == "" {
		return fmt.Errorf("to or message are required")
	}

	data, err := base.CallService("linebot", "/send", api.Request, traceID)
	if err != nil {
		return fmt.Errorf("base.CallService, %v", err)
	}

	if err := json.Unmarshal(data, &api.Response); err != nil {
		return fmt.Errorf("failed to unmarshal response, %v, data: %s", err, string(data))
	}

	if api.Response.Code != 0 {
		return fmt.Errorf("api failed, code: %d, message: %s", api.Response.Code, api.Response.Message)
	}

	return nil
}
