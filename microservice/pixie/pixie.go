package pixie

import (
	"gitlab.com/pixie-sdk/go/microservice/pixie/line"
)

type Pixie struct {
	Line line.Line
}

func NewPixie() *Pixie {
	return &Pixie{}
}
