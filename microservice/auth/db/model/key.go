package model

import "github.com/Lee-Chi/go-sdk/db/mongo"

type key struct {
	Auth struct {
		ID   mongo.K
		Name mongo.K
		Url  mongo.K
	}
}

var Key key

func init() {
	Key.Auth.ID = mongo.Key("_id")
	Key.Auth.Name = mongo.Key("name")
	Key.Auth.Url = mongo.Key("url")
}
