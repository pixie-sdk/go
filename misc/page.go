package misc

import "math"

// GetMaximumPage returns The maximum page number is based on starting from page 1.
func GetMaximumPage(count int64, pageSize int64) int64 {
	if count <= pageSize {
		return 1
	}

	return int64(math.Ceil(float64(count) / float64(pageSize)))
}
