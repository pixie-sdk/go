package base

import (
	"github.com/Lee-Chi/go-sdk/http"
)

func Post(url string, header map[string]string, data interface{}) ([]byte, error) {
	request := http.NewRequest()
	for key, value := range header {
		request.SetHeader(key, value)
	}

	return request.PostJson(url, data)
}
