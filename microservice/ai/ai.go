package ai

import (
	"encoding/json"
	"fmt"
	"mime/multipart"
	"sync"

	"gitlab.com/pixie-sdk/go/microservice/base"
)

type AI struct {
}

var ai *AI
var once sync.Once

func NewAI() *AI {
	once.Do(func() {
		ai = &AI{}
	})

	return ai
}

func (ai AI) Chat() *APIChat {
	return &APIChat{}
}

type APIChat struct {
	// request
	Request struct {
		AI             string `json:"ai"`
		ResponseFormat string `json:"responseFormat"`
		Ask            string `json:"ask"`
	}
	// response
	Response struct {
		Code    int    `json:"code"`
		Message string `json:"message"`
		Answer  string `json:"answer"`
	}
}

func (api *APIChat) UnmarshalRequest(payload []byte) *APIChat {
	json.Unmarshal(payload, &api.Request)
	return api
}

func (api *APIChat) Do(traceID string) error {
	if api.Request.Ask == "" {
		return fmt.Errorf("ask is required")
	}

	data, err := base.CallService("ai", "/api/ai/chat", api.Request, traceID)
	if err != nil {
		return fmt.Errorf("base.CallService, %v", err)
	}

	if err := json.Unmarshal(data, &api.Response); err != nil {
		return fmt.Errorf("failed to unmarshal response, %v, data: %s", err, string(data))
	}

	if api.Response.Code != 0 {
		return fmt.Errorf("api failed, code: %d, message: %s", api.Response.Code, api.Response.Message)
	}

	return nil
}

func (ai AI) Imagen() *APIImagen {
	return &APIImagen{}
}

type APIImagen struct {
	// request
	Request struct {
		AI             string                `form:"ai"`
		ResposneFormat string                `form:"responseFormat"`
		Ask            string                `form:"ask"`
		File           *multipart.FileHeader `form:"file"`
	}

	// response
	Response struct {
		Code    int    `json:"code"`
		Message string `json:"message"`
		Answer  string `json:"answer"`
	}
}

func (api *APIImagen) Do(traceID string) error {
	if api.Request.Ask == "" {
		return fmt.Errorf("ask is required")
	}

	if api.Request.File == nil {
		return fmt.Errorf("file is required")
	}

	params := map[string]string{
		"ai":             api.Request.AI,
		"responseFormat": api.Request.ResposneFormat,
		"ask":            api.Request.Ask,
	}

	files := map[string]*multipart.FileHeader{
		"file": api.Request.File,
	}

	data, err := base.CallServiceWithFile("ai", "/api/ai/imagen", params, files, traceID)
	if err != nil {
		return fmt.Errorf("base.CallServiceWithFile, %v", err)
	}

	if err := json.Unmarshal(data, &api.Response); err != nil {
		return fmt.Errorf("failed to unmarshal response, %v, data: %s", err, string(data))
	}

	if api.Response.Code != 0 {
		return fmt.Errorf("api failed, code: %d, message: %s", api.Response.Code, api.Response.Message)
	}

	return nil
}
