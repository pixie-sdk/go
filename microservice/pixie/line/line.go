package line

import (
	"gitlab.com/pixie-sdk/go/microservice/pixie/line/task"
	"gitlab.com/pixie-sdk/go/microservice/pixie/line/user"
)

type Line struct {
	User user.Group
	Task task.Group
}
