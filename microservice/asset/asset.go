package asset

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io"
	"mime/multipart"
	"net/http"
)

type Asset struct {
	uploadURL string
}

func NewAsset(url string) *Asset {
	return &Asset{
		uploadURL: url + "/asset/upload",
	}
}

func (a Asset) Upload(fileName string, file io.Reader) error {
	// Create a buffer to hold the multipart form data
	buffer := &bytes.Buffer{}

	// Create a new multipart writer to write the form data to the buffer
	formWriter := multipart.NewWriter(buffer)

	// Create a new form part for the file within the multipart form
	part, err := formWriter.CreateFormFile("file", fileName)
	if err != nil {
		return fmt.Errorf("Create form file fail, %v", err)
	}

	// Read all bytes from the provided reader (assuming the file data)
	data, err := io.ReadAll(file)
	if err != nil {
		return fmt.Errorf("Read file fail, %v", err)
	}

	// Write the file data to the created form part
	// Use _, err := ... to discard the number of bytes written (unused here)
	if _, err := io.WriteString(part, string(data)); err != nil {
		return fmt.Errorf("Write file fail, %v", err)
	}

	// Get the content type of the multipart form data
	contentType := formWriter.FormDataContentType()

	// Close the multipart writer to finalize the form data
	formWriter.Close()

	// Create a new HTTP POST request to the upload URL
	request, err := http.NewRequest("POST", a.uploadURL, buffer)
	if err != nil {
		return fmt.Errorf("Create request fail, %v", err)
	}

	// Set the Content-Type header of the request with the multipart form data type
	request.Header.Set("Content-Type", contentType)

	// Create a new HTTP client
	client := http.Client{}

	// Do the HTTP request and retrieve the response
	response, err := client.Do(request)
	if err != nil {
		return fmt.Errorf("Do request fail, %v", err)
	}

	// Close the response body if the request was successful
	if response != nil {
		defer response.Body.Close()
	}

	// Read all bytes from the response body
	body, err := io.ReadAll(response.Body)
	if err != nil {
		// Handle error reading the response body
		return fmt.Errorf("Read body fail, %v", err)
	}

	// Define a struct to hold the expected JSON res format
	var res struct {
		Code    int    `json:"code"`
		Message string `json:"message"`
	}

	// Unmarshal the JSON response body into the defined struct
	if err := json.Unmarshal(body, &res); err != nil {
		// Handle error unmarshalling the JSON response
		return fmt.Errorf("Unmarshal body fail, %v", err)
	}

	// Check if the response code indicates success (usually code 0)
	if res.Code != 0 {
		// Handle error response with message and code
		return fmt.Errorf("Response %s(%d)", res.Message, res.Code)
	}

	// Return nil if everything is successful
	return nil
}
