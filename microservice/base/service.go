package base

import (
	"fmt"
	"io"
	"mime/multipart"

	"github.com/Lee-Chi/go-sdk/http"
	"gitlab.com/pixie-sdk/go/microservice/auth"
)

func CallService(serviceName string, api string, params interface{}, traceID string) ([]byte, error) {
	serviceAuth, err := auth.Create(serviceName, false)
	if err != nil {
		return nil, fmt.Errorf("failed to get auth, %v", err)
	}

	url := fmt.Sprintf("%s%s", serviceAuth.Url, api)
	data, err := http.NewRequest().
		SetHeader("Authorization", serviceAuth.Auth).
		SetHeader("TraceId", traceID).
		PostJson(url, params)
	if err != nil {
		if err == http.ErrUnauthorized {
			serviceAuth, err = auth.Create(serviceName, true)
			if err != nil {
				return nil, fmt.Errorf("failed to reget auth, %v", err)
			}

			data, err = http.NewRequest().
				SetHeader("Authorization", serviceAuth.Auth).
				SetHeader("TraceId", traceID).
				PostJson(url, params)
			if err != nil {
				return nil, fmt.Errorf("failed to send request, %v", err)
			}
		} else {
			return nil, fmt.Errorf("failed to send request, %v", err)
		}
	}

	return data, nil
}

type File struct {
	Name string
	Data io.Reader
}

func CallServiceWithFile(serviceName string, api string, params map[string]string, files map[string]*multipart.FileHeader, traceID string) ([]byte, error) {
	serviceAuth, err := auth.Create(serviceName, false)
	if err != nil {
		return nil, fmt.Errorf("failed to get auth, %v", err)
	}

	url := fmt.Sprintf("%s%s", serviceAuth.Url, api)
	data, err := http.NewRequest().
		SetHeader("Authorization", serviceAuth.Auth).
		SetHeader("TraceId", traceID).
		PostMultipart(url, files, params)
	if err != nil {
		if err == http.ErrUnauthorized {
			serviceAuth, err = auth.Create(serviceName, true)
			if err != nil {
				return nil, fmt.Errorf("failed to reget auth, %v", err)
			}

			data, err = http.NewRequest().
				SetHeader("Authorization", serviceAuth.Auth).
				SetHeader("TraceId", traceID).
				PostMultipart(url, files, params)
			if err != nil {
				return nil, fmt.Errorf("failed to send request, %v", err)
			}
		} else {
			return nil, fmt.Errorf("failed to send request, %v", err)
		}
	}

	return data, nil
}

func CheckAuthorization(authorization string) error {
	if authorization == "" {
		return fmt.Errorf("authorization is required")
	}

	if err := auth.Verify(authorization); err != nil {
		return fmt.Errorf("failed to verify authorization, %v", err)
	}

	return nil
}
